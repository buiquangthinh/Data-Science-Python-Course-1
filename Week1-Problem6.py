# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 1
# Problem 6

def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum

print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))

# print(sum(10, 6)) --- screen: 20 (because 10 + 6 = 16 in (15,20))
# print(sum(10, 2)) --- screen: 12 (because 10 + 2 = 12 not in (15,20))
# print(sum(10, 12)) --- screen: 22 (because 10 + 12 = 22 not in (15,20))