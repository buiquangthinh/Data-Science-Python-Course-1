# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 2
# Problem 2

from datetime import date

x = input("Input the first day with format DD/MM/YYYY: ")
y = x.split("/")
z = [int(i) for i in y]

print("")

m = input("Input the second day with format DD/MM/YYYY: ")
n = m.split("/")
p = [int(i) for i in n]

xd = date(z[2], z[1], z[0])
md = date(p[2], p[1], p[0])
out = abs(xd - md)

print("")
print("The number of days among {} and {}: ".format(x, m))
print(out.days)