# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 3
# Problem 1 (Required Part)
# Array calculations and operations

import numpy as np

# No.1
a = np.array([range(4),range(10,14)])
print(a)

print("")
b = np.array([2, -1, 1, 0])
print(b)

print("")
print(a*b)

b1 = b*100
b2 = b*100.0

print("")
print(b1)
print("")
print(b2)

print("")
print(b1 == b2)

print("")
print(b1.dtype,b2.dtype)

# No.2
print("")
print("")

arr = np.array(range(10))
print(arr)

print("")
print(arr < 3)
print("")
print(np.less(arr,3))

print("")
co = np.logical_or(arr < 3, arr > 8)
print(co)

print("")
new = np.where(co, arr * 5, arr * -5)
print(new)

# No.3
print("")
print("")

def wind(a,b,mi=0.1):
    temp = ((a**2) + (b**2))**0.5
    out = np.where(temp > mi, temp, mi)
    print(out)

u1 = np.array([[4, 5, 6], [2, 3, 4]])
v1 = np.array([[2, 2, 2], [1, 1, 1]])
wind(u1,v1)

print("")
u2 = np.array([[4, 5, 0.01], [2, 3, 4]])
v2 = np.array([[2, 2, 0.03], [1, 1, 1]])
wind(u2,v2)