# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 3
# Problem 1 (Required Part)
# Interrogating and manipulating arrays

import numpy as np

# No.1
arr = np.array([range(4), range(10, 14)])
print(arr)

print("")
print(arr.shape)

print("")
print(arr.size)

print("")
print(arr.max())

print("")
print(arr.min())

# No.2
print("")
print("")

print(arr.reshape(2,2,2))

print("")
print(arr.transpose())

print("")
print(arr.ravel())

print("")
print(arr.astype("f"))