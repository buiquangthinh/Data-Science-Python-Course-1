# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 2
# Problem 1

x = input("Input a sequence of comma-separated numbers: ")
x = x.split(",")
x = [int(i) for i in x]

m1 = []
m2 = []
for i in x:
    if i % 2 == 0:
        m1.append(i)
    else:
        m2.append(i)
print("")
print("The list consists of even numbers: ")
print(m1)
print("")
print("The list consists of old numbers: ")
print(m2)