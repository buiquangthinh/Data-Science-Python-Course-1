# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 2
# Problem 3

import random
import math

n = int(input("Input number n to randomly generate a list of n integers on interval [0,10]: "))
x = []

for i in range(0, n):
    k = random.randint(0, 10)
    x.append(k)

print("")
print("A random list of {} integers on interval [0,10]: ".format(n))
print(x)


def histogram(li):
    out = ""
    for i in li:
        while (i > 0):
            out += "*"
            i -= 1
        print(out)


print("")
print("A histogram of the list: ")
histogram(x)

nhonhat = x[0]
for i in x:
    if i < nhonhat:
        nhonhat = i

print("")
print("The min of the list: ")
print(nhonhat)

lonnhat = x[0]
for i in x:
    if i > lonnhat:
        lonnhat = i

print("")
print("The max of the list: ")
print(lonnhat)

tong = 0
cou = 0
for i in x:
    cou += 1
    tong += i
m = tong / cou

print("")
print("The mean of the list: ")
print(m)

tongbinh = 0
for i in x:
    tongbinh += (i - m) ** 2
v = tongbinh / cou

print("")
print("The variance of the list: ")
print(v)

sd = math.sqrt(v)

print("")
print("The standard deviation of the list: ")
print(sd)

print("")


def remove(li):
    out = []
    for i in li:
        if i not in out:
            out.append(i)
    print(out)


print("The list after removing duplicates: ")
remove(x)