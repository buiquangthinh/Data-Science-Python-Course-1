# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 3
# Problem 3 (Required Part)

import random
import numpy as np

m = int(input("Please input m to get create 3D array: "))
n = int(input("Please input n to get create 3D array: "))
k = int(input("Please input m to get create 3D array: "))
l = m * n * k

temp = []

for i in range(1, l + 1):
    x = random.uniform(-100, 100)
    temp.append(x)
    out = np.array(temp)

print("")
arr = out.reshape((m, n, k))
print("The 3D array {} x {} x {} with random float values on interval [-100,100] is: ".format(m, n, k))
print("")
print(arr)

print("")
print("# Deep Axis")
print("The sum of array by deep axis is: ")
print(arr.sum(axis=0))
print("The min of array by deep axis is: ")
print(arr.min(axis=0))
print("The max of array by deep axis is: ")
print(arr.min(axis=0))

print("")
print("# Height Axis")
print("The sum of array by height axis is: ")
print(arr.sum(axis=1))
print("The min of array by height axis is: ")
print(arr.min(axis=1))
print("The max of array by height axis is: ")
print(arr.min(axis=1))

print("")
print("# Width Axis")
print("The sum of array by width axis is: ")
print(arr.sum(axis=2))
print("The min of array by width axis is: ")
print(arr.min(axis=2))
print("The max of array by width axis is: ")
print(arr.min(axis=2))