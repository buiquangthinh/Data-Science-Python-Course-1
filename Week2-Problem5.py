# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 2
# Problem 5

x = input("Input a matrix with dot-separated rows be sequences of comma-separated numbers: ")
x = x.split(".")

y = []
for i in range(len(x)):
    y.append([int(i) for i in x[i].split(",")])

print("")
print("The matrix is:")
print(y)

tong = 0
for i in range(len(y)):
    temp = 0
    for j in y[i]:
        temp += j
    tong += temp

print("")
print("The sum of the matrix is:")
print(tong)