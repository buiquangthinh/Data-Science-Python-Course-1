# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 3
# Problem 1 (Required Part)
# Introduction to NumPy arrays

import numpy as np

# No.1
x = range(1,11)
print(x)

print("")
a1 = np.array(x,"i")
print(a1.dtype)

print("")
a2 = np.array(x,"f")
print(a2.dtype)

# No.2
print("")
print("")

m = np.zeros((2,3,4))
print(m)

print("")
n = np.ones((2,3,4))
print(n)

print("")
p = np.arange(1000)
print(p)

# No.3
print("")
print("")

a = np.array([2, 3.2, 5.5, -6.4, -2.2, 2.4])
print(a)

print("")
print(a[1])

print("")
print(a[1:4])

print("")
b = np.array([[2, 3.2, 5.5, -6.4, -2.2, 2.4],[1, 22, 4, 0.1, 5.3, -9],[3, 1, 2.1, 21, 1.1, -2]])
print(b)

print("")
print(b[:,3])

print("")
print(b[1:4, 0:6])

print("")
print(b[1:, 2])