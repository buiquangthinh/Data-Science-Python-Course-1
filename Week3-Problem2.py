# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 3
# Problem 2 (Required Part)

import random
import numpy as np

n = int(input("Please input n to get create 3D array: "))
m = n ** 3

temp = []

for i in range(1, m + 1):
    x = random.uniform(0, 10)
    temp.append(x)
    out = np.array(temp)

print("")
arr = out.reshape((n, n, n))
print("The 3D array {} x {} x {} with random float values on interval [0,10] is: ".format(n, n, n))
print("")
print(arr)

print("")
print("# Deep Axis")
print("The sum of array by deep axis is: ")
print(arr.sum(axis=0))
print("The min of array by deep axis is: ")
print(arr.min(axis=0))
print("The max of array by deep axis is: ")
print(arr.min(axis=0))

print("")
print("# Height Axis")
print("The sum of array by height axis is: ")
print(arr.sum(axis=1))
print("The min of array by height axis is: ")
print(arr.min(axis=1))
print("The max of array by height axis is: ")
print(arr.min(axis=1))

print("")
print("# Width Axis")
print("The sum of array by width axis is: ")
print(arr.sum(axis=2))
print("The min of array by width axis is: ")
print(arr.min(axis=2))
print("The max of array by width axis is: ")
print(arr.min(axis=2))