import datetime
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))

# print ("Current date and time : ") --- screen: Current date and time :
# print (now.strftime("%Y-%m-%d %H:%M:%S")) --- screen: 'date and time when run the code' (% helps to display full form of date and time)