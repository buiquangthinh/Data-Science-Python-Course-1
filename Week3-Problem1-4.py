# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 3
# Problem 1 (Required Part)
# Working with missing values

import numpy.ma as MA

# No.1
marr = MA.masked_array(range(10), fill_value = -999)

print("")
print(marr, marr.fill_value)

print("")
marr[2] = MA.masked
print(marr)

print("")
print(marr.mask)

print("")
narr = MA.masked_where(marr > 6, marr)
print(narr)

print("")
mi = MA.filled(narr)
print(mi)

print("")
print(type(mi))

# No.2
print("")
print("")

m1 = MA.masked_array(range(1, 9))
print(m1)

print("")
m2 = m1.reshape(2, 4)
print(m2)

print("")
m3 = MA.masked_greater(m2, 6)
print(m3)

print("")
res = m3 - np.ones((2, 4))
print(res)

print("")
print(type(res))