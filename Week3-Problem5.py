# Quang-Thinh BUI
# Data-Science-Python-Course-1
# Week 3
# Problem 5 (Required Part)
# Financial functions

import numpy as np

# numpy.fv(rate, nper, pmt, pv, when='end')
# Compute the future value
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.fv.html#numpy.fv

# "What is the future value after 10 years of saving $100 now, with an additional monthly savings of $100.
# Assume the interest rate is 5% (annually) compounded monthly."

temp1 = np.fv(0.05/12, 10*12, -100, -100)
print(temp1)

# numpy.pv(rate, nper, pmt, fv=0, when='end')
# Compute the present value
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.pv.html#numpy.pv

# "What is the present value (e.g., the initial investment) of an investment that needs to total $15692.93
# after 10 years of saving $100 every month? Assume the interest rate is 5% (annually) compounded monthly."

print("")
temp2 = np.pv(0.05/12, 10*12, -100, 15692.93)
print(temp2)

# numpy.npv(rate, values)
# Returns the NPV (Net Present Value) of a cash flow series
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.npv.html#numpy.npv

print("")
temp3 = np.npv(0.281,[-100, 39, 59, 55, 20])
print(temp3)

# numpy.pmt(rate, nper, pv, fv=0, when='end')
# Compute the payment against loan principal plus interest
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.pmt.html#numpy.pmt

# "What is the monthly payment needed to pay off a $200,000 loan in 15 years at an annual interest rate of 7.5%?"

print("")
temp4 = np.pmt(0.075/12, 12*15, 200000)
print(temp4)

# numpy.ppmt(rate, per, nper, pv, fv=0, when='end')
# Compute the payment against loan principal
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.ppmt.html#numpy.ppmt

print("")
temp5 = np.ppmt(0.075/12, 2000, 12*15, 200000)
print(temp5)

# numpy.ipmt(rate, per, nper, pv, fv=0, when='end')
# Compute the interest portion of a payment
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.ipmt.html#numpy.ipmt

# "What is the amortization schedule for a 1 year loan of $2500 at 8.24% interest per year compounded monthly?"

print("")
temp6 = np.pmt(0.0824/12, 1*12, 2500)
print(temp6)

# numpy.irr(values)
# Return the Internal Rate of Return (IRR): This is the “average” periodically compounded rate of return
# that gives a net present value of 0.0; for a more complete explanation, see Notes below
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.irr.html#numpy.irr

print("")
temp7 = np.irr([-100, 39, 59, 55, 20])
print(temp7)

# numpy.mirr(values, finance_rate, reinvest_rate)
# Modified internal rate of return
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.mirr.html#numpy.mirr

# numpy.nper(rate, pmt, pv, fv=0, when='end')
# Compute the number of periodic payments
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.nper.html#numpy.nper

# "If you only had $150/month to pay towards the loan,
# how long would it take to pay-off a loan of $8,000 at 7% annual interest?"

print("")
temp8 = np.nper(0.07/12, -150, 8000)
print(temp8)

# numpy.rate(nper, pmt, pv, fv, when='end', guess=None, tol=None, maxiter=100)
# CCompute the rate of interest per period
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.rate.html#numpy.rate